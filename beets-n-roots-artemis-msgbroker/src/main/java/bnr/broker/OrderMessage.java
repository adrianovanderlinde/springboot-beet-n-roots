package bnr.broker;

import java.io.Serializable;

public class OrderMessage implements Serializable {
	
	private static final long serialVersionUID = 7526472295622776147L;

	private long orderId;
	
	private String content;
	
	public OrderMessage() {}

	public OrderMessage(long orderId, String content) {
		this.orderId = orderId;
		this.content = content;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Message [orderId=" + orderId + ", content=" + content + "]";
	}
		
	

}
