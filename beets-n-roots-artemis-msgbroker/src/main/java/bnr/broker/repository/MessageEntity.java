package bnr.broker.repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

@Entity(name="messages")
public class MessageEntity {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="order_id")
	private String orderId;
	
	@Column(name="content")
	private String content;
	
	@Column(name="create_date")
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime createDate;
	
	public MessageEntity() {}
	
	public MessageEntity(String orderId, String content) {
		this.orderId = orderId;
		this.content = content;
		this.createDate = LocalDateTime.now();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	
	@Override
    public String toString() {
        return "Message {" +
                "id='" + this.id + '\'' +
                ", order_id=" + this.orderId +
                ", content=" + this.content +
                ", date=" + this.createDate.format(DateTimeFormatter.ISO_DATE) +
                '}';
    }
}
