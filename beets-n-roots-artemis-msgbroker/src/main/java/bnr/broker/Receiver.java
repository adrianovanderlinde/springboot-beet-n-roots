package bnr.broker;

import java.util.concurrent.CountDownLatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;

import bnr.broker.repository.MessageEntity;
import bnr.broker.repository.MessageRepository;


@EnableJms
public class Receiver {
	
	@Autowired
	private Sender sender;
	
	@Autowired
	private MessageRepository messageRespository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

	private CountDownLatch latch = new CountDownLatch(1);

	public CountDownLatch getLatch() {
		return latch;
	}

	@JmsListener(destination = "orders.q")
	public void receive(String message) {
//			LOGGER.info("received message='{}'", message);
			
			try {
				int from = message.indexOf("orderId=");
				int to = message.indexOf(",");
				String orderId = message.substring(from+8, to).trim();
				
				this.messageRespository.save(new MessageEntity(orderId, message));
	
				
			} catch(Exception e) {
				sender.sendFailure(e.getMessage());
				LOGGER.info("problems {}", e.getMessage());
			}
			
			latch.countDown();
			
	}
}
