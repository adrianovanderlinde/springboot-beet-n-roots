package bnr.broker;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

@EnableJms
public class Sender {

//  private static final Logger LOGGER =
//      LoggerFactory.getLogger(Sender.class);

  @Autowired
  private JmsTemplate jmsTemplate;

  public void send(String message) {
//    LOGGER.info("sending message='{}'", message.toString());
    jmsTemplate.setDefaultDestinationName("orders.q");
    jmsTemplate.convertAndSend("orders.q", message);
  }
  
  public void sendFailure(String message) {
//    LOGGER.info("sending Failure message='{}'", message.toString());
    jmsTemplate.convertAndSend("failures.q", message);
  }
}
