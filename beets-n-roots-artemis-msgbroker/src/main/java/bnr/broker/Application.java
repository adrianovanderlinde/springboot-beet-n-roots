package bnr.broker;

import org.apache.activemq.artemis.core.config.impl.ConfigurationImpl;
import org.apache.activemq.artemis.core.server.ActiveMQServer;
import org.apache.activemq.artemis.core.server.ActiveMQServers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		initEmbeddedMQServer();
		SpringApplication.run(Application.class, args);
	}
  
	private static void initEmbeddedMQServer( ) {
		ActiveMQServer server;
		try {
			server = ActiveMQServers.newActiveMQServer(
					new ConfigurationImpl()
                                               .setPersistenceEnabled(false)
                                               .setJournalDirectory("target/data/journal")
                                               .setSecurityEnabled(false)
                                               .addAcceptorConfiguration("invm", "vm://0")
                                               .addAcceptorConfiguration("tcp", "tcp://localhost:6001"));

			server.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}	  
	}
}
