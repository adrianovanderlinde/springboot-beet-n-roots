create table messages (
	id int(11) unsigned auto_increment primary key,
	order_id varchar(255),
	content text,
	create_date timestamp
);