package bnr.websocket;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Order {
	private long Id;
	private String orderId;
	private String details;
	private long restaurantId;
	private LocalDateTime createDate;

	public Order() {}
	
	public long getId() {
		return Id;
	}
	
	public void setId(long id) {
		Id = id;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	
	public long getRestaurantId() {
		return restaurantId;
	}
	
	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
    public String toString() {
        return "Order{" +
                "orderId=" + this.orderId +
                ", restaurant=" + this.restaurantId +
                ", details=" + this.details +
                ", date=" + this.createDate.format(DateTimeFormatter.ISO_DATE_TIME) +
                '}';
    }
}
