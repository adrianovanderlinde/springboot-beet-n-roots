package bnr.websocket;

import java.util.concurrent.CountDownLatch;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.util.HtmlUtils;


@EnableJms
public class Receiver {

//	private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

	@Autowired
    private SimpMessagingTemplate template;
	
	private CountDownLatch latch = new CountDownLatch(1);

	public CountDownLatch getLatch() {
		return latch;
	}

	@JmsListener(destination = "failures.q")
	public void receive(String message) {
//			LOGGER.info("received Failure -- message='{}'", message);
			latch.countDown();
			
			this.template.convertAndSend("/orders/monitor", new Greeting(HtmlUtils.htmlEscape(message)));
//			LOGGER.info("send to socket the Failure -- message='{}'", message);
	}
}
