package bnr.websocket;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.HtmlUtils;


@EnableScheduling
@Controller
public class OrdersController {

	@Autowired
    private SimpMessagingTemplate template;
	
	@Value("${api.url}")
	private String API_URL;
	
    @MessageMapping("/hello")
    @SendTo("/orders/update")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
    
    
    @Scheduled(fixedRate = 50000)
    public void getOrders() {
    	RestTemplate restTemplate = new RestTemplate();
    	try {
			ResponseEntity<ArrayList<Order>> response = 
					restTemplate.exchange(API_URL + "/orders", HttpMethod.GET, null, new ParameterizedTypeReference<ArrayList<Order>>(){});
			
	        ArrayList<Order> orders = response.getBody();
			orders.forEach(order -> 
					{
				        this.template.convertAndSend("/orders/received/"+order.getRestaurantId(), new Greeting(HtmlUtils.htmlEscape(order.toString())));
					}
			);
    	} catch (Exception e) {
    		this.template.convertAndSend("/orders/monitor", new Greeting(HtmlUtils.htmlEscape(e.getMessage())));
    	}
    }

}
