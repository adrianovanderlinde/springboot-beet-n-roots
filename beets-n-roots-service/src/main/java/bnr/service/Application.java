package bnr.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;

import bnr.service.repository.OrderEntity;
import bnr.service.repository.OrderRepository;

import javax.jms.JMSException;


@EnableScheduling
@SpringBootApplication
public class Application {
	
	@Autowired
	private Sender sender;
	
	@Autowired
	private OrderRepository orderRespository;
	
	@Autowired
	private Sender senderJms;
	
	@Value("${api.url}")
	private String API_URL;
	
	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String args[]) {
		SpringApplication app = new SpringApplication(Application.class);
        app.setDefaultProperties(Collections
          .singletonMap("server.port", "8082"));
        app.run(args);
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Scheduled(fixedRate = 60000)
	public void getOrders() throws JMSException {
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<ArrayList<Order>> response = 
					restTemplate.exchange(API_URL + "orders", 
							HttpMethod.GET, 
							null, 
							new ParameterizedTypeReference<ArrayList<Order>>() {}
					);
			
	        ArrayList<Order> orders = response.getBody();
			
			orders.forEach(order -> 
					{
						if (order.getDetails().equalsIgnoreCase("Eisbein")) {
							sender.sendFailure("BAD: We do not offer Eisbein!!! orderId:" 
												+ order.getOrderId() + " restaurant: " + order.getRestaurantId() 
												+ "\r\n @ " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
						} else {
							this.orderRespository.save(
									new OrderEntity(order.getOrderId(), order.getDetails(), order.getRestaurantId(), "new")
									);
//							log.info(order.toString());
							// send message to the broker to log about the order
							senderJms.send(order.toString());
						}
					}
			);
		} catch (Exception e) {
			sender.sendFailure(e.getMessage());
			log.info("problems {}", e.getMessage());
		}
	}
	
	@Scheduled(fixedRate = 1800000)
	public void updateOrders() {
		this.orderRespository.updateAllOrders("done");
	}
}