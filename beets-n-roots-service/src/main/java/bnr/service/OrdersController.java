package bnr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bnr.service.repository.OrderEntity;
import bnr.service.repository.OrderRepository;


@RestController
public class OrdersController {
	
	 @Autowired
	 private OrderRepository orderRespository;
	
	@RequestMapping("/orders")
    public List<OrderEntity> getOrders() {
    	List<OrderEntity> orders = this.orderRespository.findByStatus("new");
    	
        return orders;
    }

}
