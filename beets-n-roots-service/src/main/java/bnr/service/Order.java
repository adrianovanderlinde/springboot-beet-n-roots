package bnr.service;

public class Order {
	
	private String orderId;
	private String details;
	private long restaurantId;
	
	public Order() {}
	
	public Order(String orderId, String details, long restaurantId) {
		this.orderId = orderId;
		this.details = details;
		this.restaurantId = restaurantId;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getDetails() {
		return details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public long getRestaurantId() {
		return restaurantId;
	}


	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}
	
	@Override
    public String toString() {
        return "Order{" +
                " orderId=" + this.orderId +
                ", restaurantId=" + this.restaurantId +
                ", details='" + this.details + '\'' +
                '}';
    }

}
