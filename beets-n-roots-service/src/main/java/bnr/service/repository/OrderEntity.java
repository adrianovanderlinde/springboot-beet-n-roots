package bnr.service.repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

@Entity(name="orders")
public class OrderEntity {

	@Id
	@Column(name="id")
	private long id;
	
	@Column(name="order_id")
	private String orderId;
	
	@Column(name="details")
	private String details;
	
	@Column(name="restaurant_id")
	private long restaurantId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="create_date")
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime createDate;
	
	public OrderEntity() {}
	
	public OrderEntity(String orderId, String details, long restaurantId, String status) {
		this.orderId = orderId;
		this.details = details;
		this.restaurantId = restaurantId;
		this.status = status;
		this.createDate = LocalDateTime.now();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
    public String toString() {
        return "Order{" +
                " orderId='" + this.orderId + '\'' +
                ", restaurantId=" + this.restaurantId +
                ", details=" + this.details +
                ", status=" + this.status +
                ", date=" + this.createDate.format(DateTimeFormatter.ISO_DATE_TIME) +
                '}';
    }
	
	
}
