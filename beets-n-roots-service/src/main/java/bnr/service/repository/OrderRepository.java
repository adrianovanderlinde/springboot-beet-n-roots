package bnr.service.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {
	
	
	List<OrderEntity> findByStatus(String status);
	
	@Transactional
	@Modifying(clearAutomatically = true)
    @Query(value = "UPDATE orders SET status = :status WHERE status = 'new'", nativeQuery = true)
    void updateAllOrders(@Param("status") String newStatus);
	
}
