create table orders (
	id int(11) unsigned auto_increment primary key,
	order_id varchar(255),
	details text,
	restaurant_id int(11),
	status varchar(255),
	create_date timestamp
);