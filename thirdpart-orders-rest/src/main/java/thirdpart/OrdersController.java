package thirdpart;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrdersController {
    
    @RequestMapping("/orders")
    public ArrayList<Order> getOrders() {
    	ArrayList<Order> orders = new ArrayList<>();
    	
    	Random random = new Random();
    	String[] listOfFood = {"Italian Pasta", "Indian Bowl", "Schnitzel", "Donner", "Feijoada", "Eisbein"};
    	
    	for (int i = 0; i <= random.nextInt(7)+1; i++) {
    		orders.add(new Order(UUID.randomUUID().toString(), listOfFood[random.nextInt(listOfFood.length)], random.nextInt(4-1) + 1));	
    	}
    	
        return orders;
    }
    
    
}
